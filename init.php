<?php

//error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
error_reporting(E_ALL);
ini_set('display_errors', 'on');

DEFINE("DEFAULT_TIMEZONE", "Australia/Adelaide");
DEFINE("DEFAULT_INTERVALTYPE", "D");
DEFINE("FORMAT_YEAR_DISPLAY_DECIMALS", 2);
DEFINE("STANDARD_ERROR_MESSAGE", "Error, Please enter the dates in YYYY-MM-DD format. Example 2013-12-31. <br />You can also use now - to get the current date and time.");
DEFINE("SYSTEM_ERROR_MESSAGE_01", "System unable to process your request at this time.");


function describ($var) {
    echo '<pre>';
    print_r($var);
    echo '</pre>';
    exit;
}

function describe($var) {
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}
